//Code obtained and modified from https://www.geeksforgeeks.org/udp-server-client-implementation-c/

/**
 * @author Weston Corbeil and Jake Lee
 * @date 10 September 2020
 * 
 * UDP_Corbeil_Lee_Client.c is a program that is capable of interacting
 * with any server that runs the StreamMP3 protocol. It contains featu-
 * res to list what songs are available to stream, request a specific 
 * song for streaming, and exit the program. 
 * 
 * If a request to stream a file is successful, a new .mp3 file will be
 * created and written (or overwritten if it already exists) with .mp3
 * binary data that can be played via Windows Media Player. If the req-
 * uest fails/is unsuccessful, the file that is created will be automa-
 * tically deleted by this program. It also renames the file that is 
 * created by adding a recv_ prefix in the filename. This is done so 
 * that the original file is not overwritten if you have it in the same
 * directory as the one where you are currently streaming to. 
 * 
 * DOCUMENTATION - We referred to several TutorialsPoint websites to 
 * help us remember how many of the standard C functions work including
 * the following: strcmp(), scanf(), fgets, strcat(), strcpy(), fopen()
 * and fwrite(). We also referred to a TutorialsPoint website for how
 * to clear the input stream so that we could read in an entire line 
 * including spaces. Finally, we had conceptual discussions (no code
 * involved) for how we would potentially write the buffer into the
 * newly created .mp3 file with C2C Mechalke C2C Duran. These discuss-
 * ions helped us figure out that the n variable would be the best to 
 * use for the number of bytes to write in the fwrite() function. 
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <errno.h>
#include <sys/time.h>

#define PORT 4240               // Designates the Port number of the server
#define MAXLINE 1024            // Allocates space for buffer string
#define MAX_RESPONSE_LENGTH 256 // Allocates space for user inputs

// Driver code
int main()
{

    // Variable to keep track of frame number and total bytes(Later on)
    int frameNum = 0;
    int totalBytes = 0;

    int sockfd;           //Socket descriptor, like a file-handle
    char buffer[MAXLINE]; //buffer to store message from server

    // MESSAGES TO SEND TO SERVER:
    //========================================================================================================
    char *LIST_REQUEST = "LIST_REQUEST";           // LIST_REQUEST
    char *START_STREAM = "START_STREAM\n";         // START_STREAM (Extra space added for songName parameter)
    char FULL_STREAM_REQUEST[MAX_RESPONSE_LENGTH]; // Will be concatinated with START_STREAM and songName;
    //========================================================================================================

    struct sockaddr_in servaddr; //we don't bind to a socket to send UDP traffic, so we only need to configure server address

    // Creating socket file descriptor
    if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        perror("socket creation failed");
    }

    // Filling server information
    servaddr.sin_family = AF_INET;         //IPv4
    servaddr.sin_port = htons(PORT);       // port, converted to network byte order (prevents little/big endian confusion between hosts)
    servaddr.sin_addr.s_addr = INADDR_ANY; //localhost

    int n, len = sizeof(servaddr);

    // Set the timeout for the socket (This was example code given to us in the writeup)
    struct timeval timeout;
    timeout.tv_sec = 5;
    timeout.tv_usec = 0;
    if (setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
    {
        perror("setsocketopt failed");
    }

    // Strings to keep track of user inputs when interacting with client application
    char userSelection[MAX_RESPONSE_LENGTH];
    char songName[MAX_RESPONSE_LENGTH / 2];

    // BEGIN THE MAIN CLIENT PROGRAM LOOP (The user will have multiple opportunities to interact with the client program)
    do
    {
        // List possible options for the user
        printf("Enter one of the following commands:\n");
        printf("\"1\" = List Songs\n");
        printf("\"2\" = Stream a Song\n");
        printf("\"3\" = exit\n");

        // Get the user's input for what service they would like
        scanf("%s", userSelection);

        // ================================
        // IF THE USER SELECTS LIST_REQUEST
        // ================================

        if (strcmp(userSelection, "1") == 0)
        {

            // Automatically send the LIST_REQUEST command to the server
            sendto(sockfd, (const char *)LIST_REQUEST, strlen(LIST_REQUEST), 0, (const struct sockaddr *)&servaddr, sizeof(servaddr));
            printf("Requesting a list of songs\n");

            // ===========================
            // Receive message from server
            // ===========================

            // If receiving the message failed:
            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            if ((n = recvfrom(sockfd, (char *)buffer, MAXLINE, 0, (struct sockaddr *)&servaddr, &len)) < 0)
            {
                buffer[n] = '\0'; //terminate message
                perror("Error receiving response from server");
                printf("Errno: %d. ", errno);
            }

            // If receiving the message succeeded:
            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            else {
                // ADD NULL CHARACTER TO THE END OF THE BUFFER STRING
                buffer[n] = '\0'; //terminate message
                printf("Songs Available: %s\n", (buffer + 10));
            }
        }

        // ===============================
        // IF THE USER SELECTS STREAM_DATA
        // ===============================

        else if (strcmp(userSelection, "2") == 0)
        {
            strcpy(songName, "\0"); // Resetting Song Name so that it doesn't carry over from previous iterations in the loop!
            printf("Please enter a song name: ");

            // Clear the input stream buffer (Otherwise it thinks we already input a string here)
            while ((getchar()) != '\n');

            // Using fgets allows us to enter an entire line including spaces for the song name
            fgets(songName, MAX_RESPONSE_LENGTH, stdin);

            // Ensure there is a null character at the end of songname (this is probably redundant)
            songName[strlen(songName) - 1] = '\0';

            // Concatinate the stream request with the songname so that the server recognizes it as a proper command
            strcpy(FULL_STREAM_REQUEST, START_STREAM);
            strcat(FULL_STREAM_REQUEST, songName);

            // Create a new file or overwrite if the file already exists
            char fileName[MAX_RESPONSE_LENGTH] = "recv_";
            FILE *fp;
            fp = fopen(strcat(fileName, songName), "wb");

            // Reset data stats
            frameNum = 0;
            totalBytes = 0;

            // Send the start stream request to the server
            printf("Sending start stream\n");
            sendto(sockfd, (const char *)FULL_STREAM_REQUEST, strlen(FULL_STREAM_REQUEST), 0, (const struct sockaddr *)&servaddr, sizeof(servaddr));
            printf("Waiting for response\n");

            // =======================
            // Receive the STREAM_DATA
            // =======================

            do
            {
                // ===================
                // If you receive data
                // ===================

                // If the recvfrom() function fails:
                if ((n = recvfrom(sockfd, (char *)buffer, MAXLINE, 0, (struct sockaddr *)&servaddr, &len)) < 0)
                {
                    // Tell user that the server timed out
                    printf("Server timed out!\n");

                    // Close the file pointer and delete the file we attempted to create
                    fclose(fp);
                    remove(fileName);

                    // Leave the inner loop if there is a timeout
                    break;
                }
                buffer[n] = '\0'; //terminate message

                // =========================================================================
                // Check if data is finished being streamed, if not, write data to .mp3 file
                // =========================================================================

                // If data == "STREAM_DONE" :
                //>>>>>>>>>>>>>>>>>>>>>>>>>>>
                if (strcmp(buffer, "STREAM_DONE") == 0){

                    // Print data stats to console and close the file
                    printf("Stream done. Total Frames: %d Total Size: %d bytes\n", frameNum, totalBytes);
                    fclose(fp);
                    printf("Done!\n");

                    // Leave inner loop
                    break;
                }

                // If data == "COMMAND_ERROR" :
                //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                else if (strcmp(buffer, "COMMAND_ERROR") == 0){
                    // Let user know error occurred
                    printf("Command Error received from server.  Cleaning up...\n");

                    // Close file pointer and delete the file we attempted to create
                    fclose(fp);
                    remove(fileName);
                    printf("Done!\n");

                    // Leave inner loop
                    break;
                }

                // IF data == regular data :
                //>>>>>>>>>>>>>>>>>>>>>>>>>>
                else {
                    // Calculate data statistics and print to screen
                    frameNum++;
                    totalBytes += n;
                    printf("Frame # %d received with %d bytes\n", frameNum, n - 12);

                    // Write the data to the open file
                    fwrite(buffer + 12, 1, n - 12, fp);

                    // NOTE: The inner loop will continue until all the datagrams have been received
                }

                // buffer != "STREAM_DONE" or buffer != "COMMAND_ERROR"
            } while (strcmp(buffer, "STREAM_DONE") != 0 || strcmp(buffer, "COMMAND ERROR") != 0);
        }

        // ========================
        // IF THE USER SELECTS EXIT
        // ========================

        else if (strcmp(userSelection, "3") == 0)
        {

            // Close the socket, tell the user the program is exiting, and return normal program value
            close(sockfd);
            printf("Exiting");
            return 0;
        }

        // ==========================================
        // IF THE USER DOES NOT INPUT VALID SELECTION
        // ==========================================

        else
        {
            printf("You did not enter a valid selection. Please enter the whole number 1, 2, or 3.");
        }

        // Allow the loop to reset if no valid input was found
    } while (strcmp(userSelection, "3") != 0);

    // Just in case the socket didn't get closed the first time...
    close(sockfd);
    return 0;
}